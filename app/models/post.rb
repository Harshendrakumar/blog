class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy         #Primary Key
  validates :title, presence: true                #Mandatory attribute title
  validates :title, uniqueness: true              #Post must be unique
  validates :body, length: {maximum: 500}         #Maximum lengh of body
  validates :body, presence: true                 #Mandatory attribute post:body
end
