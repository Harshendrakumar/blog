class Comment < ActiveRecord::Base
  belongs_to :post                              #Foreign Key
  validates :post_id, presence: true            #Mandatory attribute post_id
  validates :body, presence: true               #Mandatory attribute body
  validates :body, length: {maximum: 500}       #Maximum lenght of the body
  #Comment must have a post 
  validates_presence_of :post                     
end
